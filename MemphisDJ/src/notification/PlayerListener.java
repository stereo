package notification;

public interface PlayerListener extends Listener {
	
	public void playbackStarted();
	public void playbackFinished();
	
}
