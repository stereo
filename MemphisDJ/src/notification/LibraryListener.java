package notification;


public interface LibraryListener extends Listener {

	public void libraryVersionChanged(int version);
	
}
